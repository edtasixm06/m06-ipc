# /usr/bin/python
#-*- coding: utf-8-*-
#
# exemple-execv.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys,os
print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid !=0:
  print("Programa Pare", os.getpid(), pid)
  sys.exit(0)
# ------------------------------------
print("Programa fill", os.getpid(), pid)
#os.execv("/usr/bin/ls",["/usr/bin/ls","-ls","/"])
#os.execl("/usr/bin/ls","/usr/bin/ls","-ls","/")
#os.execlp("ls","ls","-ls","/")
#os.execvp("uname",["uname","-a"])
#os.execv("/bin/bash",["/bin/bash","show.sh"])
os.execle("/bin/bash","/bin/bash","show.sh",{"nom":"joan","edat":"25"})





print("Hasta lugo lucas!")
sys.exit(0)

