# Automatització de tasques

 - Exercicis de repàs de python

 - IPC

## @ edt ASIX M06 2022-2023

Podeu trobar els programes al GitLab de [edtasixm06](https://gitlab.com/edtasixm06/ipc-2021-22.git
)

Podeu trobar la documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)

ASIX M06-ASO Escola del treball de barcelona


#### Repàs Python / Argsparse / Objectes

 * [Python documentation](https://docs.python.org/3/library/argparse.html)
 * [Argparse tutorial](https://docs.python.org/3/howto/argparse.html)



