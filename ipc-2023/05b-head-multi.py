#! /usr/bin/python3
#-*- coding: utf-8-*-
# @edt ASIX M06 Curs 2023-2024
#
# head [-n 5|10|15] -v file...  
#  default=10
# ------------------------------
import sys, argparse
parser = argparse.ArgumentParser(description=\
  """Mostrar les N primereslínies """,\
  epilog="thats all folks")
parser.add_argument("-n","--nlin",type=int,\
  help="Número de línies 5,(10),15",dest="nlin",\
  metavar="numLines",choices=[5,10,15],default=10)
parser.add_argument("fileList",\
  help="fitxer a processar", metavar="file",\
  nargs="*")
parser.add_argument("-v", "--verbose",action="store_true",default=False)
args=parser.parse_args()
print(args)
# -------------------------------
MAX=args.nlin

def headFile(fitxer):
  fileIn=open(fitxer,"r")
  counter=0
  for line in fileIn:
    counter+=1     
    print(line,end="")
    if counter==MAX: break
  fileIn.close()

if args.fileList:
  for fileName in args.fileList:
    if args.verbose:  print("\n",fileName, 40*"-")  
    headFile(fileName)

exit(0)


