# /usr/bin/python3
#-*- coding: utf-8-*-
#
# exemple-fork.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2023-2024
# Gener 2024
# -------------------------------------
import sys,os
print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid !=0:
    print("Programa pare: ",os.getpid(), pid)
    sys.exit(0)

# programa fill
#os.execv("/usr/bin/ls", ["/usr/bin/ls", "-la","/"])
#os.execl("/usr/bin/ls","/usr/bin/ls","-la","/")
#os.execlp("ls","ls","-la","/")
#os.execvp("uname",["uname","-a"])
#os.execv("/bin/bash",["/bin/bash", "show.sh"])
os.execle("/bin/bash","/bin/bash","show.sh",{"nom":"joan","edat":"25"}) 

print("Hasta lueg lucas!")
sys.exit(0)

