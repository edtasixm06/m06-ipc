# /usr/bin/python3
#-*- coding: utf-8-*-
#
# signal.py segons
# -------------------------------------
# @ edt ASIX M06 Curs 2023-2024
# Abril 2024
# -------------------------------------
import sys, os, signal, argparse
parser = argparse.ArgumentParser(description="Gestinar alarma")
parser.add_argument("segons", type=int, help="segons")
args=parser.parse_args()
print(args)
# -------------------------------------
up=0
down=0

def myusr1(signum, frame):
  global up
  print("Signal handler with signal: ", signum)
  actual=signal.alarm(0)
  signal.alarm(actual+60)
  up+=1

def myusr2(signum, frame):
  global down  
  print("Signal handler with signal:", signum)
  actual=signal.alarm(0)
  if actual > 60:
    signal.alarm(actual-60)
  else:
    print("ignored, not enough time ",actual)
    signal.alarm(actual)  
  down+=1

def myalarm(signum,frame):
  global up, down
  print("Signal handler called with signal:", signum)
  print("Finalitzant... up: %d down:%d restant: %d" % (up, down, signal.alarm(0)))
  sys.exit(0)

def myterm(signum,frame):   
  print("Signal handler called with signal:", signum)
  falten=signal.alarm(0)
  signal.alarm(falten)
  print("Falten actualment %d segons" % (falten))

def myhup(signum,frame):
  print("Signal handler called with signal:", signum)
  print("Restoring value: ", args.segons)
  signal.alarm(args.segons)


# Assignar un handler al senyal
signal.signal(signal.SIGUSR1,myusr1)          #10
signal.signal(signal.SIGUSR2,myusr2)          #12
signal.signal(signal.SIGALRM,myalarm)         #14
signal.signal(signal.SIGTERM,myterm)          #15
signal.signal(signal.SIGHUP,myhup)            #1
signal.signal(signal.SIGINT,signal.SIG_IGN)   #2

signal.alarm(args.segons)
print(os.getpid())
while True:
  pass    
sys.exit(0)



