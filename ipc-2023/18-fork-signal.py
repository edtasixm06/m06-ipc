# /usr/bin/python3
#-*- coding: utf-8-*-
#
# fork-signal.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2023-2024
# Gener 2024
# -------------------------------------
import sys, os, signal

def myuser1(signum,frame):
  print("Signal handler called with signal:", signum)
  print("Hola radiola!")

def myuser2(signum,frame):
  print("Signal handler called with signal:", signum)
  print("Aedeu andreu!")  
  sys.exit(0)

# --------------------------------------
print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())
pid=os.fork()
if pid !=0:
    #os.wait()
    print("Programa pare: ",os.getpid(), pid)
    print("Llançant el procés fill servidor")
    sys.exit(0)
    
print("Programa fill: ", os.getpid(), pid)
signal.signal(signal.SIGUSR1, myuser1)
signal.signal(signal.SIGUSR2, myuser2)
while True:
    pass

# això no s'executarà mai
print("Hasta lueg lucas!")
sys.exit(0)

