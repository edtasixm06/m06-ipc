# /user/bin/python3
#-*- coding: utf-8-*-
# @ Abril 2024
# popen_sql.py  -c num_cli [-c numcli...]
# ------------------------------
import sys, argparse
from subprocess import Popen, PIPE
# -------------------------------
parser = argparse.ArgumentParser(description=\
        """Consulta a la base de dades entrada per argument al programa.""")
parser.add_argument('-c',"--client", help='client',type=str,\
        action="append",dest="clieList",required="True")
args=parser.parse_args()
# print(args)
# for num_clie in args.clieList:
#   print(num_clie)
# exit(0)

# -------------------------------
command = "PGPASSWORD=passwd  psql -qtA -F',' -h localhost  -U postgres training"
pipeData = Popen(command, shell=True, bufsize=0, \
             universal_newlines=True,
        	 stdin=PIPE, stdout=PIPE, stderr=PIPE)

for num_clie in args.clieList:
    sqlStatement="select * from clientes where num_clie=%s;" % ( num_clie )
    pipeData.stdin.write(sqlStatement+"\n")
    print(pipeData.stdout.readline(), end="")
pipeData.stdin.write("\q\n")
exit(0)

