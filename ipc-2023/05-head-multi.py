#! /usr/bin/python3
#-*- coding: utf-8-*-
# @edt ASIX M06 Curs 2023-2024
#
# head [-n 5|10|15] [-f file]...  
#  default=10, file o stdin
# ------------------------------
# $ head.py -n 2 -f file.txt
# $ head.py < file.txt
# $ head.py -n 3
# $ head.py -f file.txt
# ------------------------------
import sys, argparse
parser = argparse.ArgumentParser(description=\
  """Mostrar les N primereslínies """,\
  epilog="thats all folks")
parser.add_argument("-n","--nlin",type=int,\
  help="Número de línies 5,(10),15",dest="nlin",\
  metavar="numLines",choices=[5,10,15],default=10)

parser.add_argument("-f","--fit",\
  help="fitxer a processar (stdin)", metavar="file",\
  dest="fileList",action="append")
args=parser.parse_args()
print(args)

# -------------------------------
MAX=args.nlin

def headFile(fitxer):
  fileIn=open(itxer,"r")
  counter=0
  for line in fileIn:
    counter+=1     
    print(line,end="")
    if counter==MAX: break
  fileIn.close()

for fileName in args.fileList:
  headFile(fileName)

exit(0)


