#!/usr/bin/python
#-*- coding: utf-8-*-
'''
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# @edt Curs 2018-2019
# Echo server multiple-connexions
# -----------------------------------------------------------------
'''
import socket, sys, select
from subprocess import Popen, PIPE
HOST = ''                 
PORT = 50001             
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)
conns=[s]
while True:
    actius,x,y = select.select(conns,[],[])
    for actual in actius:
        if actual == s:
            conn, addr = s.accept()
            print 'Connected by', addr
            conns.append(conn)
        else:
            data = actual.recv(1024)
            if not data:
                sys.stdout.write("Client finalitzat: %s \n" % (actual))
                actual.close()
                conns.remove(actual)
            else:
				command=data
				pipeData = Popen(command,shell=True,stdout=PIPE,stderr=PIPE)
				for line in pipeData.stdout:
					#if DEBUG: sys.stderr.write(line)
					conn.sendall(line)
				for line in pipeData.stderr:
					#if DEBUG: sys.stderr.write(line)
					conn.sendall(line)
				conn.sendall(chr(4))
                #actual.sendall(data)
                #actual.sendall(chr(4),socket.MSG_DONTWAIT)
s.close()
sys.exit(0)
