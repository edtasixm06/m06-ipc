# /usr/bin/python
#-*- coding: utf-8-*-
# telnet-client.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys,socket,argparse
parser = argparse.ArgumentParser(description="""telnet client""")
parser.add_argument("server",type=str)
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()
HOST = args.server
PORT = args.port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.connect((HOST, PORT))
while True:
  command = raw_input("telnet> ")
  if not command: break
  s.sendall(command)
  while True:
    data = s.recv(1024)
    if data[-1]==chr(4):
      print data[:-1]  
      break
    print data,
s.close()
sys.exit(0)


