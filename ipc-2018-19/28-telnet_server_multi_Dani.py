# /usr/bin/python
#-*- coding: utf-8-*-
#
# exemple-echoClient.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys,socket,argparse, signal,os,datetime,select
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description=\
        """CAL server""",\
        epilog="thats all folks")
parser.add_argument("-d","--debug",type=int,\
        help="ser debug",dest="debug",\
        metavar="debug",choices=[0,1],default=0)
parser.add_argument("-p","--puerto",type=int,\
        help="numero de puerto", metavar="puerto",\
        default=50001,dest="puerto")
args=parser.parse_args()
# -------------------------------------------------------
def mysigusr1(signum,frame):
	global upp
	print "Signal handler called with signal:", signum
	print llistaPeers	
	sys.exit(0)

def mysigusr2(signum,frame):
	print "Signal handler called with signal:", signum
	print len(llistaPeers)
	sys.exit(0)

def mysigterm(signum,frame):
	print "Signal handler called with signal:", signum
	print llistaPeers,len(llistaPeers)
	sys.exit(0)
# -------------------------------------------------------

    
pid=os.fork()
if pid!=0:
	print "Engegat el server:",pid
	sys.exit(0)
	  
signal.signal(signal.SIGUSR1,mysigusr1)
signal.signal(signal.SIGUSR2,mysigusr2)
signal.signal(signal.SIGTERM,mysigterm)

llistaPeers=[]
HOST = ''
PORT = args.puerto
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)
conns=[s]
while True:
	actius,x,y = select.select(conns,[],[])
	for actual in actius:
		#si es una conneccio nova
		if actual == s:
			conn, addr = s.accept()
			print 'Connected by', addr
			conns.append(conn)
		else:
			data = actual.recv(1024)
            #sortirà quan el client tanquí la conecció
			if not data:
				sys.stdout.write("Client finalitzat: %s \n" % (actual))
				actual.close()
				conns.remove(actual)
			else:
				command = [data]
				pipeData = Popen(command,stdout=PIPE)
				for line in pipeData.stdout:
					conn.send(line)
				conn.send(chr(4))
s.close()
sys.exit(0)


