# /usr/bin/python
#-*- coding: utf-8-*-
#
# exemple-execv.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys,os
print "Hola, començament del programa principal"
print "PID pare: ", os.getpid()

pid=os.fork()
if pid !=0:
  print "Programa Pare", os.getpid(), pid
  sys.exit(0)

print "Programa fill", os.getpid(), pid
#os.execv("/usr/bin/ls",["/usr/bin/ls","-ls","/"])
#os.execl("/usr/bin/ls","/usr/bin/ls","-la","/")
#os.execlp("ls","ls","-la","/")
os.execve("/usr/bin/ls",["/usr/bin/ls","-la","/"],{"nom":"/tmp","edat":"15"})
print "Hasta lugo lucas!"
sys.exit(0)


