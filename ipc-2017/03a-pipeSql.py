#!/usr/bin/python
#-*- coding: utf-8-*-
# @edt ASIX-M06 Curs 2017-2018
# Popen amb consulta SQL 
#   a) tot hardcoded
#
# Utilitza el docker edtasixm06/postgres
# -----------------------------------------
import sys
from subprocess import Popen, PIPE

commandLocal = "psql -qtA -F ';' lab_clinic \
                     -c 'select * from pacients;'"
#commandRemote = "psql -qtA -h i11 -U postgres -F ';' lab_clinic \
#                      -c 'select * from pacients;'"
commandRemote = "psql -h 172.17.0.2 -U edtasixm06  training \
                      -c 'select *  from oficinas;'"
pipeData = Popen(commandRemote, shell = True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
for line in pipeData.stdout.readlines():
  print line,
sys.exit(0)

