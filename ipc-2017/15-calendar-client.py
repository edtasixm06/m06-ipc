#!/usr/bin/python
#-*- coding: utf-8-*-
# @edt ASIX-M06 Curs 2017-2018
# Calendar client.py
# Synopsis: -s host -p port [-y year]
#------------------------------------

import sys, os, argparse, socket

# Creació arguments
parser = argparse.ArgumentParser(description="calendar.py -s server -p port [-y year]")
parser.add_argument("-s", "--server", dest="hostConnect", required=True)
parser.add_argument("-p", "--port", dest="portConnect", type=int, required= True) 
parser.add_argument("-y", "--year", dest="calYear",default="2018")

args = parser.parse_args()
HOST = args.hostConnect
PORT = args.portConnect
YEAR = args.calYear

# Creació socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
s.send(YEAR)
while True:
	# Rep les dades
	data = s.recv(1024)
        print data
	if not data: break
s.close()
sys.exit(0)

