#!/usr/bin/python
#-*- coding: utf-8-*-
# @edt ASIX-M06 Curs 2017-2018
# Popen amb consulta SQL 
#   Amb diàleg pare - subproces per demanar client a client
#
# Utilitza el docker edtasixm06/postgres
# -----------------------------------------
import sys
from subprocess import Popen, PIPE
import argparse

listClie=None
parser = argparse.ArgumentParser(description='Consulta SQL interactiva')
parser.add_argument("-c","--client", help='codi num_clie', metavar='NUM_CLIE', action='append',dest='listClie')
args = parser.parse_args()
print args

cmd = "psql -qt -h 172.17.0.2 -U postgres training"
pipeData = Popen(cmd, shell = True, stdin=PIPE, stdout=PIPE, stderr=PIPE)

for clie in args.listClie:
  sqlStatment="select * from clientes where num_clie=%s;\n" % (clie)
  pipeData.stdin.write(sqlStatment)
  data=pipeData.stdout.readline()
  if len(data) != 1:
    print data,
    data=pipeData.stdout.readline()
  else:
    sys.stderr.write("clie:%s not found\n" % (clie))
pipeData.stdin.write("\q\n")
sys.exit(0)


