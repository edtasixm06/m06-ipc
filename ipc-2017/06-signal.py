#!/usr/bin/python
#-*- coding: utf-8-*-
# @edt ASSIX-M06 Curs 2017-2018
#
# Exemple IPC: Signals
# Synopsis: programa minuts
# deifineix alarma(n minuts). siguser1 incrementa 1 minut, 
# siguser2 decrementa1 minut, sighub reinicia el compte, 
# sigterm mostra quants segons falten de alarma, 
# no és interrumpible amb control c. 
# En acabar mostra el valor de upper (quants hem incrementat)
# i down (quants decrementat)
# ------------------------------------
import signal, os, sys, argparse

parser = argparse.ArgumentParser(description="Gestionar l'alarma")
parser.add_argument("minuts", type=int, help="minuts")
args=parser.parse_args()
print parser
print os.getpid()
global upper
global down
upper = 0
down = 0

def mysigusr1(signum,frame):
  print "Signal handler called with signal:", signum
  global upper
  upper+=1
  segons=signal.alarm(0)
  print segons
  signal.alarm(segons+60)

def mysigusr2(signum,frame):
  print "Signal handler called with signal:", signum
  global down
  down+=1
  segons=signal.alarm(0)
  print segons
  signal.alarm(segons-60)
 
def mysighup(signum,frame):
  print "Signal handler called with signal:", signum
  signal.alarm(args.minuts*60)

def mysigalarm(signum,frame):
  print "Signal handler called with signal:", signum
  segons=signal.alarm(0)
  print segons
  signal.alarm(segons)

def mysigterm(signum,frame):
  global upper, down
  print "Signal handler called with signal:", signum
  print args.minuts, signal.alarm(0), upper, down
  sys.exit(0)

signal.signal(signal.SIGALRM,mysigalarm)
signal.signal(signal.SIGTERM,mysigterm)
signal.signal(signal.SIGUSR1,mysigusr1)
signal.signal(signal.SIGUSR2,mysigusr2)
signal.signal(signal.SIGHUP,mysighup)
signal.signal(signal.SIGINT, signal.SIG_IGN)
signal.alarm(args.minuts*60)

while True:
  # fem un open que es queda encallat
  pass

signal.alarm(0)
sys.exit(0)


