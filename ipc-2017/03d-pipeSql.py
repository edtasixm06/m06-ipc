#!/usr/bin/python
#-*- coding: utf-8-*-
# @edt ASIX-M06 Curs 2017-2018
# Popen amb consulta SQL 
#   a) tot hardcoded
#   b) diàleg procés pare - subprocés
#   c) SQL INJECTAT -----> FATAL!!!!
#   d) consulta per num_empl INJECTAT FATAL!!! proveu:
#       "112;drop table repventas;"
#
# Utilitza el docker edtasixm06/postgres
# -----------------------------------------
#commandLocal = "psql -qtA -F ';' lab_clinic -c 'select * from pacients;'"
#commandRemote = "psql -qtA -h i11 -U postgres -F ';' lab_clinic -c 'select * from pacients;'"
# -------------------------------------------
import sys
from subprocess import Popen, PIPE
import argparse

parser = argparse.ArgumentParser(description='Consulta SQL interactiva')
parser.add_argument('numempl', help='Sentència SQL a executar',metavar='sentènciaSQL')
args = parser.parse_args()

cmd = "psql -h 172.17.0.2 -U postgres  training"
sqlStatment="select * from repventas where num_empl=%s;" % (args.numempl)

pipeData = Popen(cmd, shell = True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
pipeData.stdin.write(sqlStatment+"\n\q\n")

for line in pipeData.stdout:
  print line,
sys.exit(0)

