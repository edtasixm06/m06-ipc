#!/usr/bin/python
#-*- coding: utf-8-*-
# @edt ASIX-M06 Curs 2017-2018
#
# Exemples exercicis argparser
# Consulteu Argparser_Exercicis.pdf
#
# -----------------------------------------------------------------------------
#
# Nota: Consulteu el tutorial accessible des 15.4 argparser del Library Reference,
#       cal consultar primr l'enllaç al turorial i llavors la documentació 
# -----------------------------------------------------------------------------


import sys
import argparse
parser = argparse.ArgumentParser(description="Exemple nargs")
parser.add_argument("-r", "--rang", nargs=2)
args = parser.parse_args()
print args
print parser.parse_args("-r 0 10 ".split())
print parser.parse_args("-r 5 ".split())
print parser.parse_args("".split())
sys.exit(0)

# $ ./argparse-exercicis.py  -r 1 3
# Namespace(rang=['1', '3'])
# Namespace(rang=['0', '10'])
# usage: argparse-exercicis.py [-h] [-r RANG RANG]
#argparse-exercicis.py: error: argument -r/--rang: expected 2 argument(s)


# -----------------------------------------------------------------------------
# Exemple Extra: action append
# -----------------------------------------------------------------------------
import sys
import argparse
parser = argparse.ArgumentParser(description="Exemple action=append")
parser.add_argument("-n", "--nom", action="append")
args = parser.parse_args()
print args
print parser.parse_args("-n pere -n pau -n anna".split())
print parser.parse_args("-n pau ".split())
print parser.parse_args("".split())
sys.exit(0)

# $ ./argparse-exercicis.py  -n mama --nom papa
# Namespace(nom=['mama', 'papa'])
# Namespace(nom=['pere', 'pau', 'anna'])
# Namespace(nom=['pau'])
# Namespace(nom=None)


# -----------------------------------------------------------------------------
# Exemple Extra: nargs
# -----------------------------------------------------------------------------
import sys
import argparse
parser = argparse.ArgumentParser(description="Exemple nargs")
parser.add_argument("client", nargs="+")
args = parser.parse_args()
print args
print parser.parse_args("c1 c2 c3 ".split())
print parser.parse_args("c1".split())
print parser.parse_args("".split())
sys.exit(0)
# $ ./argparse-exercicis.py  client
# Namespace(client=['client'])
# Namespace(client=['c1', 'c2', 'c3'])
# Namespace(client=['c1'])
# usage: argparse-exercicis.py [-h] client [client ...]
# argparse-exercicis.py: error: too few arguments


# -----------------------------------------------------------------------------
# Exemple Extra: action count
# -----------------------------------------------------------------------------
import sys
import argparse
parser = argparse.ArgumentParser(description="Exemple action=count")
parser.add_argument("-p", "--present", action="count", default=0)
parser.add_argument("-v", action="count")
args = parser.parse_args()
print args
print parser.parse_args("-pp ".split())
print parser.parse_args("-v -v -p -vv -pp".split())
print parser.parse_args("--present -v".split())
print parser.parse_args("-v -vv".split())
print parser.parse_args("".split())
sys.exit(0)

# $ ./argparse-exercicis.py  -vvvv --present
# Namespace(present=1, v=4)
# Namespace(present=2, v=None)
# Namespace(present=3, v=4)
# Namespace(present=1, v=1)
# Namespace(present=0, v=3)
# Namespace(present=0, v=None)


# -----------------------------------------------------------------------------
# Exemple Extra: action store_true/store_false
# -----------------------------------------------------------------------------
import sys
import argparse
parser = argparse.ArgumentParser(description="Exemple action=store_true/false")
parser.add_argument("-p", "--present", action="store_true")
parser.add_argument("-v", action="store_true")
args = parser.parse_args()
print args
print parser.parse_args("-p".split())
print parser.parse_args("-v".split())
print parser.parse_args("--present -v".split())
print parser.parse_args("".split())
print parser.parse_args("--help".split())
sys.exit(0)

# $ ./argparse-exercicis.py  -v
# Namespace(present=False, v=True)
# Namespace(present=True, v=False)
# Namespace(present=False, v=True)
# Namespace(present=True, v=True)
# Namespace(present=False, v=False)
# usage: argparse-exercicis.py [-h] [-p] [-v]
# Exemple action=store_true/false
# optional arguments:
#  -h, --help     show this help message and exit
#  -p, --present
#  -v


# -----------------------------------------------------------------------------
# 7. Valida la següent synopsis que rep obligatoriament els fitxers d’usuaris i grups per
# processar-los segons el criteri per defecte login.
# Synopsis: programa [-s login | uid | gid | gname ] usuaris grups.
# -----------------------------------------------------------------------------
import sys
import argparse
parser = argparse.ArgumentParser(description="sort_usuris [ -s login|uid|gid|gname] -u usuaris -g grups")
parser.add_argument("-s", "--sort", choices=["login", "uid", "gid", "gname"], default="login")
parser.add_argument("usuaris")
parser.add_argument("grups")
args = parser.parse_args()
print args
print parser.parse_args("userfile groupfile".split())
print parser.parse_args("-s gname ufile gfile".split())
print parser.parse_args("".split())
sys.exit(0)

# $ ./argparse-exercicis.py  fuser  fgroup
# Namespace(grups='fgroup', sort='login', usuaris='fuser')
# Namespace(grups='groupfile', sort='login', usuaris='userfile')
# Namespace(grups='gfile', sort='gname', usuaris='ufile')
# usage: argparse-exercicis.py [-h] [-s {login,uid,gid,gname}] usuaris grups
# argparse-exercicis.py: error: too few arguments


# -----------------------------------------------------------------------------
# 6. Valida la següent synopsis que rep obligatoriament les opcions dels fitxers d’usuaris i
# grups per processar-los segons el criteri per defecte login .
# Sinopsys: programa [-s login | uid | gid | gname ] -u usuaris -g grups.
# -----------------------------------------------------------------------------
import sys
import argparse
parser = argparse.ArgumentParser(description="sort_usuris [ -s login|uid|gid|gname] -u usuaris -g grups")
parser.add_argument("-s", "--sort", choices=["login", "uid", "gid", "gname"], default="login")
parser.add_argument("-u", "--usuaris", required=True)
parser.add_argument("-g", "--grups", required=True)
args = parser.parse_args()
print args
print parser.parse_args("-u userfile -g groupfile".split())
print parser.parse_args("-s gid -u userfile -g groupfile".split())
print parser.parse_args("".split())
sys.exit(0)

# $ ./argparse-exercicis.py -u fuser -g fgroup
# Namespace(grups='fgroup', sort='login', usuaris='fuser')
# Namespace(grups='groupfile', sort='login', usuaris='userfile')
# Namespace(grups='groupfile', sort='gid', usuaris='userfile')
# usage: argparse-exercicis.py [-h] [-s {login,uid,gid,gname}] -u USUARIS -g GRUPS
# argparse-exercicis.py: error: argument -u/--usuaris is required


# -----------------------------------------------------------------------------
# 5. Valida la següent synopsis on per defecte es processa stdin amb el criteri
# d’ordenació per login .
# Synopsis: sort_usuaris [-s login|uid|name] [-f fitxer]
# -----------------------------------------------------------------------------
import sys
import argparse
parser = argparse.ArgumentParser(description="sort_usuris [ -s login|uid|name]  [-f file ]")
parser.add_argument("-s", "--sortopc", choices=["login", "uid", "name"], default="login")
parser.add_argument("-f", "--fitxer", default="/dev/stdin")
args = parser.parse_args()
print args
print parser.parse_args("-f nomfile".split())
print parser.parse_args("-s name".split())
print parser.parse_args("-f nom --sort uid".split())
print parser.parse_args("".split())
print parser.parse_args("--sort homedir".split())
sys.exit(0)

# $ ./argparse-exercicis.py --fitxer filenom --sort uid
# Namespace(fitxer='filenom', sortopc='uid')
# Namespace(fitxer='nomfile', sortopc='login')
# Namespace(fitxer='/dev/stdin', sortopc='name')
# Namespace(fitxer='nom', sortopc='uid')
# Namespace(fitxer='/dev/stdin', sortopc='login')
# usage: argparse-exercicis.py [-h] [-s {login,uid,name}] [-f FITXER]
# argparse-exercicis.py: error: argument -s/--sortopc: invalid choice: 'homedir' (choose from 'login', 'uid', 'name')


# -----------------------------------------------------------------------------
# 4. Valida la següent synopsis, on per defecte es processen les 10 últimes línies de
# stdin.
# Synopsis: tail.py [-f fitxer]... [-n 5 | 10 | 15 ]
# -----------------------------------------------------------------------------
import sys
import argparse
parser = argparse.ArgumentParser(description="tail.py [-f file ] [-n linies] ")
parser.add_argument("-f", "--fitxer", type=str, default="/dev/stdin")
parser.add_argument("-n", "--numlin", type=int, choices=[5, 10, 15], default=10)
args = parser.parse_args()
print args
print parser.parse_args("-f nomfile".split())
print parser.parse_args("-n 15".split())
print parser.parse_args("-f nom --numlin 15".split())
print parser.parse_args("".split())
#print parser.parse_args("-f".split())
print parser.parse_args("--numlin 12".split())
sys.exit(0)

# $ ./argparse-exercicis.py -f nom -n 5
# Namespace(fitxer='nom', numlin=5)
# Namespace(fitxer='nomfile', numlin=10)
# Namespace(fitxer='/dev/stdin', numlin=15)
# Namespace(fitxer='nom', numlin=15)
# Namespace(fitxer='/dev/stdin', numlin=10)
# usage: argparse-exercicis.py [-h] [-f FITXER] [-n {5,10,15}]
# argparse-exercicis.py: error: argument -n/--numlin: invalid choice: 12 (choose from 5, 10, 15)


# -----------------------------------------------------------------------------
# 3. Valida la següent synopsis, on per defecte es processen les 10 últimes línies de
# stdin  .
# Synopsis: tail.py [-f fitxer] [-n línies]
# ----------------------------------------------------------------------------
import sys
import argparse
parser = argparse.ArgumentParser(description="tail.py [-f file ] [-n linies] ")
parser.add_argument("-f", "--fitxer", type=str, default="/dev/stdin")
parser.add_argument("-n", "--numlin", type=int, default=10)
args = parser.parse_args()
print args
print parser.parse_args("-f nomfile".split())
print parser.parse_args("-n 15".split())
print parser.parse_args("-f nom --numlin 6".split())
print parser.parse_args("".split())
print parser.parse_args("-f".split())
sys.exit(0)

# $ ./argparse-exercicis.py -f nom -n 23
# Namespace(fitxer='nom', numlin=23)
# Namespace(fitxer='nomfile', numlin=10)
# Namespace(fitxer='/dev/stdin', numlin=15)
# Namespace(fitxer='nom', numlin=6)
# Namespace(fitxer='/dev/stdin', numlin=10)
# usage: argparse-exercicis.py [-h] [-f FITXER] [-n NUMLIN]
# argparse-exercicis.py: error: argument -f/--fitxer: expected one argument


# -----------------------------------------------------------------------------
# 2) Fer un tail mostrant les 5 últimes línies del fitxer rebut com a argument. Si no es rep cap 
# fitxer es processa la entrada stàndard. s’utilitza 5. 
# Sinopsys: tail.py [­-f fitxer] 
# -----------------------------------------------------------------------------
import sys
import argparse
parser = argparse.ArgumentParser(description="tail.py [-f file ]")
parser.add_argument("-f", "--fitxer", type=str, default="/dev/stdin")
args = parser.parse_args()
print args
print parser.parse_args("-f nomfile".split())
print parser.parse_args("-f nom1 -f nom2".split())
print parser.parse_args("".split())
print parser.parse_args("-f".split())
sys.exit(0)

# Namespace(fitxer='/dev/stdin')
# Namespace(fitxer='nomfile')
# Namespace(fitxer='nom2')
# Namespace(fitxer='/dev/stdin')
# usage: argparse-exercicis.py [-h] [-f FITXER]
# argparse-exercicis.py: error: argument -f/--fitxer: expected one argument

# -----------------------------------------------------------------------------
# 1. Valida la següent synopsis, on per defecte es processa  stdin  .
# Synpsis: $ head [file]
# -----------------------------------------------------------------------------
import sys
import argparse
parser = argparse.ArgumentParser(description="head [file]")
parser.add_argument("fitxer", type=str, nargs='?', default="/dev/stdin")
args = parser.parse_args()
print args
print parser.parse_args("/etc/fstab".split())
print parser.parse_args("".split())
sys.exit(0)

# $ ./argparse-exercicis.py nomfile
# Namespace(fitxer='nomfile')
# Namespace(fitxer='/etc/fstab')
# Namespace(fitxer='/dev/stdin')

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# tipus file
import sys
import argparse
parser = argparse.ArgumentParser(description="head [file]")
parser.add_argument("fitxer", type=file, nargs='?', default="/dev/stdin")
args = parser.parse_args()
print args
print parser.parse_args("")
print parser.parse_args("/etc/fstab".split())
print parser.parse_args("kaka.buf".split())
sys.exit(0)

# $ ./argparse-exercicis.py /etc/passwd
# Namespace(fitxer=<open file '/etc/passwd', mode 'r' at 0xb706e548>)
# Namespace(fitxer=<open file '/dev/stdin', mode 'r' at 0xb706e5a0>)
# Namespace(fitxer=<open file '/etc/fstab', mode 'r' at 0xb706e5a0>)
# Traceback (most recent call last):


