#!/usr/bin/python
#-*- coding: utf-8-*-
# @edt ASSIX-M06 Curs 2017-2018
#
# Exemple: fork/signal
# Un pare fgenera un fill i es mor.
# El fill executa el programa: 06-signal.py
# ----------------------------------
import os, sys

print "Inici del programa principal PARE"
pid = os.fork()
if pid != 0:
  print "Fi Programa PARE", os.getpid(), pid
  sys.exit(0)
print "Programa FILL", os.getpid(), pid
os.execv("/usr/bin/python",["/usr/bin/python","06-signal.py","3"])

