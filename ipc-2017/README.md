# ASIX-M06 IPC Inter Process Comunication
## @edt ASIX-M06 Curs 2017-2018
### UF2 Automatització d'Scripts

Podeu trobar la web de l'asignatura **ASIX M06-ASO Administració de sistemes operatius** a l'adreça https://sites.google.com/site/asixm06edt/ (o posant ASIX-M06 al google). A la web de **GitHub** del compte **edtasixm06** podeu trobar els dockerfiles per generar les imatges treballades. I en la web de **DockerHub** del compte **edtasixm06** i trobareu les imatges. En la web de **GitLab** del compte **edtasixm06** trobareu exemples de programació en Python de l'assignatura.

 * Web ASIX-M06 de l'assignatura https://sites.google.com/site/asixm06edt
 * GitLab amb exercicis de programació https://gitlab.com/edtasixm06
 * GitHub amb els dockerfiles https://github.com/edtasixm06
 * DockerHub amb les imatges https://hub.docker.com/u/edtasixm06/


# Treballar amb git

Aprendre a treballar amb git usant repositoris locals i repositoris remots. Definir diversos repositoris remots. Treballar l'accés remot via https i ssh.

## Desenvolupament bàsic 

 * git init / git clone
 * git add .
 * git status
 * git commit
 * git log

## Repositoris remots

 * git remote (definir varis origens)
 * git push / git pull
 * access via https
 * acces via ssh

# Argparser

 * Objecte argparse. opcions del constructor
 * Analitzar (parser). L'espai de noms retornat.
 * Positional Parameters
 * optional Parameters

# Conceptes IPC Inter Process Comunication
## Pipes
## Signals
## fork/execv
## Sockets
