#!/usr/bin/python
#-*- coding: utf-8-*-
# @edt ASSIX-M06 Curs 2017-2018
#
# Exemple IPC: Signals
# ------------------------------------

import signal, os, sys
def myhandler(signum,frame):
  print "Signal handler called with signal:", signum
  print "hasta luegu lucas"
  sys.exit(1)

def mydeath(signum,frame):
  print "Signal handler called with signal:", signum
  print "que te mueras tu!!!"


signal.signal(signal.SIGALRM,myhandler)
signal.signal(signal.SIGTERM,mydeath)
signal.alarm(60)

while True:
  # fem un open que es queda encallat
  pass

signal.alarm(0)
sys.exit(0)


