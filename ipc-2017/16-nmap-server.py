#!/usr/bin/python
#-*- coding: utf-8-*-
# @edt ASSIX-M06 Curs 2017-2018
#
# Nmap server
#  - accepta clients one by one
#  - li envien un volcat nmap i el desa a un file per a cada connexió.
#  - nomfile: ipclient-yyymmddhhmmss.log
# ----------------------------------------------------------------------
import sys, os, argparse, socket, time
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="nmap.py [-p port]")
parser.add_argument("-p", "--port", type=int, default=50001) 
args = parser.parse_args()
HOST = ""
PORT = args.port
print os.getpid()

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)
while True:
  conn, addr = s.accept()
  ip, port = addr
  sys.stderr.write("Connected by %s" % ip)
  fileName = "%s-%s.log" % (ip, time.strftime("%Y%m%d%H%M%s"))
  fileOut = open(fileName,"w")
  while True:
	data = conn.recv(1024)
        if not data: break
	fileOut.write(data)
  fileOut.close()
  conn.close()
sys.exit(0)





