#!/usr/bin/python
#-*- coding: utf-8-*-
# @edt ASSIX-M06 Curs 2017-2018
#
# Exemple: fork/execv
# generar subprocessos
# ------------------------------
import os, sys
print "Inici del programa principal PARE"
pid = os.fork()
if pid != 0:
  print os.wait()
  print "Programa PARE", os.getpid(), pid
else:
  print "Programa FILL", os.getpid(), pid
print "Fi del programa", os.getpid()
sys.exit(0)

