#!/usr/bin/python
#-*- coding: utf-8-*-
# @edt ASSIX-M06 Curs 2017-2018
#
# Exemple: fork/signal
# Un pare fgenera un fill i es mor.
# El fill fa bucle infinit, però:
#   - acaba als 3 minuts
#   - amb sihhub torna a començar els tres minuts
#   - amb sigterm mostra missatge (segons restants) i plega
# ----------------------------------
import os, sys, signal

def mysighup(signum,frame):
  print "Signal handler called with signal:", signum
  signal.alarm(3*60)
def mysigterm(signum,frame):
  print "Signal handler called with signal:", signum
  print signal.alarm(0), "bye bye!"
  sys.exit(0)
signal.signal(signal.SIGTERM,mysigterm)
signal.signal(signal.SIGHUP,mysighup)
signal.alarm(3*60)

# -----------------------------------------------
print "Inici del programa principal PARE"
pid = os.fork()
if pid != 0:
  print "Fi Programa PARE", os.getpid(), pid
  sys.exit(0)

# ----------------------------------------------
print "Programa FILL", os.getpid(), pid
while True:
  # fem un servidor
  pass
sys.exit(0)





