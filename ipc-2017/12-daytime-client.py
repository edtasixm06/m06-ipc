#!/usr/bin/python 
#-*- coding: utf-8-*-
# exemple de daytime client
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisx2 M06-ASO UF2NF1-Scripts
# @edt Curs 2017-2018  Febrer 2018
# -----------------------------------------------------------------
import sys,socket
HOST = ''
PORT = 50002
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
while True:
  data = s.recv(1024)
  if not data: break
  print data
s.close()
sys.exit(0)
