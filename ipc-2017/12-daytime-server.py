#!/usr/bin/python 
#-*- coding: utf-8-*-
# exemple de daytime server
#   - usar un popen per fer l'ordre date
#   - servidor aten un únic client, respon i tanca la connexió
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisx2 M06-ASO UF2NF1-Scripts
# @edt Curs 2017-2018  Febrer 2018
# -----------------------------------------------------------------
import os,sys,socket
from subprocess import Popen, PIPE
HOST = ''
PORT = 50002
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)
conn, addr = s.accept()
print 'Connected by', addr
command = ["/usr/bin/date"]
pipeData = Popen(command, stdout=PIPE, stderr=PIPE)
for line in pipeData.stdout:
  print line
  conn.send(line)
conn.close()
sys.exit(0)
