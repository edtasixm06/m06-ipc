#!/usr/bin/python
#-*- coding: utf-8-*-
# @edt ASSIX-M06 Curs 2017-2018
#
# Telnet server
#  connecta al server i executa ordre a ordre
#  OneByOne. No plega mai.
# ----------------------------------
import sys, os, argparse, socket
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="telnet.py [-d] [-p port]")
parser.add_argument("-d", "--debug", action="store_true") 
parser.add_argument("-p", "--port", type=int, default=50001) 
args = parser.parse_args()
HOST = ""
PORT = args.port
FI = chr(4)
print os.getpid()
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)

while True:
  conn, addr = s.accept()
  ip, port = addr
  sys.stderr.write("Connected by %s" % ip)
  while True:
    command = conn.recv(1024)
    if args.debug: sys.stderr.write(command)
    if not command:
      break
    pipeData = Popen(command, shell = True, stdout=PIPE, stderr=PIPE)
    for line in pipeData.stdout:
	  if args.debug: sys.stderr.write(line)
	  conn.sendall(line)
    for line in pipeData.stderr:
	  if args.debug: sys.stderr.write(line)
	  conn.sendall(line)
    conn.sendall(FI)
  conn.close()
s.close()
sys.exit(0)






