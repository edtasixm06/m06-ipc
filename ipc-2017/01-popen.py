#!/usr/bin/python
#-*- coding: utf-8-*-
# @edt ASIX-M06
# exemple de popen hardcoded
# ------------------------------------------
import sys
from subprocess import Popen, PIPE
import argparse

parser = argparse.ArgumentParser(description='%prog [options] [arg]\ntype %prog -h or %prog --help per a mes informacio')
parser.add_argument(dest='ruta', help='Ruta a llistar',metavar='ruta-a-llistars')
args = parser.parse_args()
print args
print parser

command=["ls",args.ruta]
p=Popen(command, stdout=PIPE, stderr=PIPE)
for line in p.stdout:
  print line, 
sys.exit(0)


