# /usr/bin/python3
#-*- coding: utf-8-*-
#
# daytime-client-one2one.py  
#
# $ python3 22-daytime-client-one2one.py -s profef2g -p 13
# $ python3 22-daytime-client-one2one.py -p 50001
# $ python3 22-daytime-client-one2one.py 
# $ python3 22-daytime-client-one2one.py -s profef2g
# -------------------------------------
# @ edt ASIX M06 Curs 2022-2023
# Gener 2023
# -------------------------------------
import sys,socket, argparse
parser = argparse.ArgumentParser(description="""Client """)
parser.add_argument("-s","--server",type=str, default='')
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()
# ---------------------------------------------------------
HOST = args.server
PORT = args.port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST,PORT))
while True:
  data = s.recv(1024)
  if not data: break
  print('data', repr(data))
s.close()
sys.exit(0)

