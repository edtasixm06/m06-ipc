# /usr/bin/python3
#-*- coding: utf-8-*-
#
# -------------------------------------
# @ edt ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
# Exemple de programació Objectes POO
# -------------------------------------
class UnixUser():
  """Classe UnixUser: /etc/passwd login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self, l, i, g):	
    "Constructor objectes UnixUser"
    self.login=l
    self.uid=i
    self.gid=g
  def show(self):
    print(self.login)
    print(self.uid)
    print(self.gid)
  def __str__(self):
    return "%s %d %d " % (self.login, self.uid, self.gid)

print("Programa")

user1=UnixUser("pere",1000,100)
user1.show()
print(user1)

exit(0)


