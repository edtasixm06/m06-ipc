# /usr/bin/python
#-*- coding: utf-8-*-
#
# popen-sql.py consulta-sql
# -------------------------------------
# @ edt ASIX M06 Curs 2022-2023
# Gener 2023
# -------------------------------------
import sys, argparse
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description='Consulta SQL interactiva')
parser.add_argument('sqlStatment', help='Sentència SQL a executar',metavar='sentènciaSQL')
args = parser.parse_args()
# -------------------------------------------------------
cmd = " psql -qtA -F',' -h localhost  -U postgres training"
pipeData = Popen(cmd, shell = True, bufsize=0, universal_newlines=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
#pipeData.stdin.write("select * from oficinas;\n\q\n")
pipeData.stdin.write(args.sqlStatment+"\n\q\n")
for line in pipeData.stdout:
  print(line, end="")
exit(0)

