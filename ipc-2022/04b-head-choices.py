# /usr/bin/python3
#-*- coding: utf-8-*-
#
# head [-n 5|10|15] file
#  10 lines, file o stdin
# -------------------------------------
# @ edt ASIX M06 Curs 2022-2023
# Gener 2023
# -------------------------------------
import sys, argparse
parser = argparse.ArgumentParser(description=\
        """Mostrar les N primereslínies """,\
        epilog="thats all folks")
parser.add_argument("-n","--nlin",type=int,\
        help="Número de línies",dest="nlin",\
        metavar="5|10|15", choices=[5,10,15],default=10)
parser.add_argument("fitxer",type=str,\
        help="fitxer a processar", metavar="file")
args=parser.parse_args()
print(args)
# -------------------------------------------------------
MAXLIN=args.nlin
fileIn=open(args.fitxer,"r")
counter=0
for line in fileIn:
  counter+=1
  print(line, end='')
  if counter==MAXLIN: break
fileIn.close()
exit(0)


