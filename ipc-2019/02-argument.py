#/usr/bin/python3
import argparse
parser=argparse.ArgumentParser(description=\
  "programa exemple de processar arguments",prog="02-arguments.py",epilog="hasta luegu lucas!")
parser.add_argument("-e","--edat", type=int, dest="useredat", help="edat a processar")
parser.add_argument("-f","--fit", type=str, help="fitxer a processar", metavar="fileIn", dest="fitxer")
args=parser.parse_args()
print(parser)
print(args)
print(args.fitxer, args.useredat)
exit(0)
